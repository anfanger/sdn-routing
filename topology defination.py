
from mininet.topo import Topo

class MyTopo( Topo ):
    "Simple topology example."

    def __init__( self ):
        "Create custom topo."

        # Initialize topology
        Topo.__init__( self )

        # Add hosts and switches
        leftHost = self.addHost( 'h1', mac='00:00:00:00:00:1a' )
        rightHost = self.addHost( 'h2', mac='00:00:00:00:00:1b' )
        centerHost = self.addHost( 'h3', mac='00:00:00:00:00:1c')
        leftServer = self.addHost( 'r1', mac='00:00:00:00:00:2a')
        rightServer = self.addHost( 'r2', mac='00:00:00:00:00:2b')
        centerServer = self.addHost( 'r3', mac='00:00:00:00:00:2c')
        leftSwitch = self.addSwitch( 's3' )
        rightSwitch = self.addSwitch( 's4' )
        centerSwitch = self.addSwitch( 's5' )
 

        # Add links
        self.addLink( leftHost, leftSwitch )
        
        self.addLink( rightSwitch, rightHost )

        self.addLink( centerHost, centerSwitch )

        self.addLink( rightSwitch, leftSwitch )
 
        self.addLink( centerSwitch, leftSwitch )

        self.addLink( centerSwitch, rightSwitch )

        self.addLink( leftServer, leftSwitch )

        self.addLink( rightServer, rightSwitch )

        self.addLink( centerServer, centerSwitch )


topos = { 'mytopo': ( lambda: MyTopo() ) }
